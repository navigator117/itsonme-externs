itsonme-externs
==============
   	
externs files used in It's On Me project for closure compiler.

bluebird_externs.js

oneforall_externs.js

HELP
--------------

[Source code] (https://bitbucket.org/navigator117/itsonme-externs.git)