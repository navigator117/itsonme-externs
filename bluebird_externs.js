/// Core

/**
 * @param {function} resolve
 * @param {function=} reject
 * @constructor
 */
var Promise = function(resolve, reject) {};

/**
 * @param {function=} fulfilledHandler
 * @param {function=} rejectedHandler
 * @return {Promise}
 */
Promise.prototype.then = function(fulfilledHandler, rejectedHandler) {};

/**
 * @param {function=} fulfilledHandler
 * @param {function=} rejectedHandler
 * @return {Promise}
 */
Promise.prototype.spread = function(fulfilledHandler,rejectedHandler) {};

/**
 * @param {...function} handler
 * @return {Promise}
 */
Promise.prototype.caught = function(handler) {};

/**
 * @param {function=} rejectedHandler
 * @return {Promise}
 */
Promise.prototype.error = function(rejectedHandler) {};

/**
 * @param {function} handler
 * @return {Promise}
 */
Promise.prototype.lastly = function(handler) {};

/**
 * @param {*} thisArg
 * @return {Promise}
 */
Promise.prototype.bind = function(thisArg) {};

/**
 * @param {...function} handler
 * @return {Promise}
 */
Promise.join = function(handler) {};

/**
 * @param {...function} fn args context
 * @return {Promise}
 */
Promise.attempt = function(fn) {};

/**
 * @param {function} fn
 * @return {function}
 */
Promise.method = function(fn) {};

/**
 * @param {*} value
 * @return {Promise}
 */
Promise.resolve = function(value) {};

/**
 * @param {*} reason
 * @return {Promise}
 */
Promise.reject = function(reason) {};

/**
 * @param {*} thisArg
 * @return {Promise}
 */
Promise.bind = function(thisArg) {};

/// Synchronous inspection

/**
 * @return {boolean}
 */
Promise.prototype.isFulfilled = function() {};

/**
 * @return {boolean}
 */
Promise.prototype.isRejected = function() {};

/**
 * @return {boolean}
 */
Promise.prototype.isPending = function() {};

/**
 * @return {*}
 */
Promise.prototype.value = function() {};

/**
 * @return {*}
 */
Promise.prototype.reason = function() {};

/**
 * @return {Promise}
 */
Promise.prototype.reflect = function() {};

/// Collections

/**
 * @return {Promise}
 */
Promise.prototype.all = function() {};

/**
 * @return {Promise}
 */
Promise.prototype.props = function() {};

/**
 * @return {Promise}
 */
Promise.prototype.settle = function() {};

/**
 * @return {Promise}
 */
Promise.prototype.any = function() {};

/**
 * @return {Promise}
 */
Promise.prototype.race = function() {};

/**
 * @param {number} count
 * @return {Promise}
 */
Promise.prototype.some = function(count) {};

/**
 * @param {function} mapper
 * @param {!object=} options
 * @return {Promise}
 */
Promise.prototype.map = function(mapper, options) {};

/**
 * @param {function} reducer
 * @param {*=} initialValue
 * @return {Promise}
 */
Promise.prototype.reduce = function(reducer, initialValue) {};

/**
 * @param {function} filterer
 * @param {!object=} options
 * @return {Promise}
 */
Promise.prototype.filter = function(filterer, options) {};

/**
 * @param {function} iterator
 * @return {Promise}
 */
Promise.prototype.each = function(iterator) {};

/// Resource management
/**
 * @param {...function} handler
 * @return {Promise}
 */
Promise.using = function(handler) {};

/**
 * @param {function} disposer
 * @return {*}
 */
Promise.prototype.disposer = function(disposer) {};

/// Promisification

/**
 * @param {function} nodeFunction
 * @param {*} receiver
 * @return {function}
 */
Promise.promisify = function(nodeFunction, receiver) {};

/**
 * @param {!object} target
 * @param {object=} options
 * @return {object}
 */
Promise.promisifyAll = function(target,options) {};

/**
 * @param {function=} callback
 * @param {object=} options
 * @return {Promise}
 */
Promise.prototype.nodeify = function(callback, options) {};

/// Timers

/**
 * @param {number} ms
 * @return {Promise}
 */
Promise.prototype.delay = function(ms) {};

/**
 * @param {number} ms
 * @param {string=} message
 * @return {Promise}
 */
Promise.prototype.timeout = function(ms, message) {};

/**
 * @param {...} ms
 * @return {Promise}
 */
Promise.delay = function(ms) {};

/// Cancellation

/**
 * @return {Promise}
 */
Promise.prototype.cancellable = function() {};

/**
 * @return {Promise}
 */
Promise.prototype.uncancellable = function() {};

/**
 * @param {*} reason
 * @return {Promise}
 */
Promise.prototype.cancel = function(reason) {};

/**
 * @return {boolean}
 */
Promise.prototype.isCancellable = function() {};

/// Generators

/**
 * @param {function} generatorFunction
 * @return {function}
 */
Promise.coroutine = function(generatorFunction) {};

/**
 * @param {function} handler
 */
Promise.coroutine.addYieldHandler = function(handler) {};

/// Utility

/**
 * @param {function} handler
 * @return {Promise}
 */
Promise.prototype.tap = function(handler) {};

/**
 * @param {string} propertyName
 * @param {...} arg
 * @return {Promise}
 */
Promise.prototype.call = function(propertyName, arg) {};

/**
 * @param {string} propertyName
 * @return {Promise}
 */
Promise.prototype.get = function(propertyName) {};

/**
 * @param {*} value
 * @return {Promise}
 */
Promise.prototype.thenReturn = function(value) {};

/**
 * @param {*} reason
 * @return {Promise}
 */
Promise.prototype.thenThrow = function(reason) {};

/**
 * @return {Object}
 */
Promise.noConflict = function() {};

/**
 * @param {function} scheduler
 */
Promise.setScheduler = function(scheduler) {};

/// Built-in error types

/**
 * @constructor
 */
var OperationalError = function() {};

/**
 * @constructor
 */
var TimeoutError = function() {};

/**
 * @constructor
 */
var CancellationError = function() {};

/**
 * @constructor
 */
var AggregateError = function() {};

/// Error management configuration

/**
 * @param handler
 */
Promise.onPossiblyUnhandledRejection = function(handler) {};

/**
 * @param handler
 */
Promise.onUnhandledRejectionHandled = function( handler) {};

Promise.longStackTraces = function() {};

/**
 * @param {function} fulfilledHandler
 * @param {function} rejectedHandler
 */
Promise.done = function(fulfilledHandler, rejectedHandler) {};