/**
 * @type {object}
 */
window.plugins = {};

/**
 * @type {object}
 */
window.plugins.OneForAll = {};

/**
 * @enum {string}
 */
window.plugins.OneForAll.constants = {
    LOGIN_INFO_OPENID: "openid",
    LOGIN_INFO_ACCESS_TOKEN: "access_token",
    LOGIN_INFO_EXPIRES_IN: "expires_in",

    LOGIN_ERROR_CANCELED: "canceled",
    LOGIN_ERROR_CODE: "code",
    LOGIN_ERROR_MESSAGE: "message",
    LOGIN_ERROR_DETAIL: "detail"
};

/**
 * @param {object} userInfo
 * @param {function=} successCallback
 * @param {function=} errorCallback
 */
window.plugins.OneForAll.setUserInfo = function(userInfo, successCallback, errorCallback) {};

/**
 * @param {function=} successCallback
 * @param {function=} errorCallback
 */
window.plugins.OneForAll.login = function(successCallback, errorCallback) {};

/**
 * @param {function=} successCallback
 * @param {function=} errorCallback
 */
window.plugins.OneForAll.logout = function(successCallback, errorCallback) {};
